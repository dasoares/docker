## API

Python Flask

l'IP de la file est en dur dans le code, pas réussi à la gérer autrement
```
192.168.1.80:5000/api
``` 

endpoints: 

GET  /api
POST /api/computations
GET  /api/computations/<id>
GET  /api/operations/<id>

pour faire une demande de calcul :
```sh
$ curl --header "Content-Type: application/json" --request POST --data  '{"a": "6", "b":"6", "op":"add"}'  192.168.1.80:5000/api/computations
{"location":"/operations/4bda954008cebd4286d543a6323fd5ef","message":"sent to queue"}
``` 

les opérations supportés sont *add*, *substract*, *divide* et *multiply*

## File d'attente

RabbitMQ

## Serveur de calcul

Python

ici aussi l'adresse de la file est en dur.

## Base de donnée

Les données sont stockée dans une base de donnée SQLite. Le répertoire contenant la base est partagé avec le conteneur du serveur de calcul et de l'API.

```sh
$ sqlite3 database.sqlite
sqlite> .tables
result
sqlite> select * from result;
1|15.5|done
2||in queue
3||in progress
4||error
```

les valeurs déjà en base sont des valeurs de test.
Pour remettre la base de donnée à son état initial qui est celui ci-dessus:
```sh
sqlite> .read setup.sql
Error: near line 15: CHECK constraint failed: CHK_Progress
```

l'erreur est volontaire et teste une contrainte.

