import os, sqlite3, hashlib, pika, time
from flask import Flask, jsonify, request


app = Flask(__name__)
path = '/database'


# Open DB
def open_db():
    return sqlite3.connect(path + '/database.sqlite')


# Close DB
def close_db(con):
    con.close();


# Push Queue 
def push_queue(id, a, b, op):
    message = '{"id": "%s", "a":"%s", "b":"%s", "op":"%s"}' % (id, a, b, op)

    con = pika.BlockingConnection(pika.ConnectionParameters('192.168.1.80'))
    chan = con.channel()
    
    chan.queue_declare(queue='queue')
    chan.basic_publish(exchange='', routing_key='queue', body=message)

    con.close()


# API State
@app.route('/api', methods=['GET'])
def welcome():
    return jsonify({"status": "api working"})


# Request
@app.route('/api/computations', methods=['POST'])
def compute():
    operations = { "add": "+", "substract": "-", "divide": "/", "multiply": "*" }
    try:
        a = request.get_json()['a']
        b = request.get_json()['b']
        op = request.get_json()['op']

        # Check if numbers
        float(a)
        float(b)

        # Test operation
        if op not in operations:
            return jsonify({"message": "Unknown or unsupported operation"}), 400
    except KeyError:
        # Test parameters
        return jsonify({"message": "Missing parameter"}), 400
    except ValueError:
        # Test parameters value
        return jsonify({"message": "One or both values are not numbers"}), 400
        

    id = hashlib.md5((a + operations[op] + b).encode()).hexdigest()

    con = open_db()
    cur = con.cursor()

    cur.execute("SELECT * FROM result WHERE id = ?", [id])
    row = cur.fetchone()

    if row is None or row[2] == 'error':
        # Not in db
        cur.execute("INSERT INTO result VALUES(?, NULL, 'in queue')", [id])
        con.commit()

        out = jsonify({"message":"sent to queue", "location":"/operations/" + id})

        push_queue(id, a, b, op)
    else:
        if row[2] == 'in queue':
            tmp = 'already in queue'
        elif row[2] == 'in progress':
            tmp = 'already being computed'
        else:
            tmp = 'already computed'
        out = jsonify({"message":tmp, "location":"/computations/" + id})
   
    close_db(con)
    return out, 202


# Result
@app.route('/api/computations/<id>', methods=['GET'])
def result(id):
    con = open_db()
    
    cur = con.cursor()
    cur.execute("SELECT * FROM result WHERE id = ?", [id])
    row = cur.fetchone()
    close_db(con)

    if row is None:
        # 404 Not Found
        return jsonify({"message": "not found"}), 404
    elif row[2] != 'done':
        # 404 Not Found
        return jsonify({"message": "no result yet"}), 404
    else:
        # 200 OK
        return jsonify({"id": id, "result": row[1]}), 200


# Status
@app.route('/api/operations/<id>', methods=['GET'])
def status(id):
    con = open_db()

    cur = con.cursor()
    cur.execute("SELECT * FROM result WHERE id = ?", [id])
    row = cur.fetchone()
    close_db(con)

    if row is None:
        # 404 Not Found
        return jsonify({"message": "not found"}), 404
    elif row[2] != 'done':
        return jsonify({"message": row[2]}), 200
    else:
        return jsonify({"location": "/computations/" + id, "message": "done"}), 303


if __name__ == '__main__':
    try:
        app.run(host='0.0.0.0', port=os.getenv('PORT'))
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
