import pika, sys, os, sqlite3, json, hashlib, time

path = '/database'
operations = { "add": "+", "substract": "-", "divide": "/", "multiply": "*" }


# Open DB
def open_db():
    return sqlite3.connect(path + '/database.sqlite')


# Close DB
def close_db(con):
    con.close()


# Backend Checks
def verify(a, b, op):
    try:
        # Check if a & b are numbers
        float(a)
        float(b)

        # Check if operation is known
        if op not in operations:
            return False
    except:
        return False;
    return True


# Check if id exists in DB
def exists_in_db(cur, id):
    cur.execute("SELECT * FROM result WHERE id = ?", [id])
    row = cur.fetchone()
    return row is not None


# Computation
def do_operation(id, a, b, op):
    con = open_db()
    cur = con.cursor()

    if exists_in_db(cur, id):
        cur.execute("UPDATE result SET progress = 'in progress' WHERE id = ?", [id])
        con.commit()

        result = eval(a + operations[op] + b)
        cur.execute("UPDATE result SET value = ?, progress = 'done' WHERE id = ?", (str(result), id))
        con.commit()
    close_db(con)


# Errors
def errors(id):
    con = open_db()
    cur = con.cursor()

    if exists_in_db(cur, id):
        cur.execute("UPDATE result SET value = NULL, progress = 'error' WHERE id = ?", [id])
    else:
        cur.execute("INSERT INTO result VALUES(?, NULL, 'error')", [id])

    con.commit()
    close_db(con)


# Computation job
def compute(data):
    try:
        json_data = json.loads(data.decode())
        id = json_data['id']
        a  = json_data['a']
        b  = json_data['b']
        op = json_data['op']
    except:
        json_data = None

    # Check if correct JSON data are given
    if json_data is not None:
        # Check if id or values are differents
        if hashlib.md5((a + operations[op] + b).encode()).hexdigest() == id:
            if(verify(a, b, op)):
                do_operation(id, a, b, op)
            else:
                errors(id)


# Main
def main():
    def callback(ch, method, properties, body):
        compute(body)

    con = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.1.80'))
    chan = con.channel()

    chan.queue_declare(queue='queue')
    
    chan.basic_consume(queue='queue', on_message_callback=callback, auto_ack=True)
    chan.start_consuming()
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
