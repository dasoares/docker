DROP TABLE IF EXISTS result;

CREATE TABLE result(
	id VARCHAR2(40) PRIMARY KEY,
	value REAL,
	progress VARCHAR2(15) NOT NULL,
	CONSTRAINT CHK_Progress CHECK(progress IN ('in queue', 'in progress', 'done', 'error'))
);

-- TESTING
INSERT INTO result VALUES('1', 15.5, 'done');
INSERT INTO result VALUES('2', NULL, 'in queue');
INSERT INTO result VALUES('3', NULL, 'in progress');
INSERT INTO result VALUES('4', NULL, 'error');
INSERT INTO result VALUES('5', 5, 'toto'); -- To verify the contraint
